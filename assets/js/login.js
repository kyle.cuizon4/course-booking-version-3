let loginForm = document.querySelector('#logInUser');


loginForm.addEventListener('submit', e =>{
	e.preventDefault();

	let email = document.querySelector('#userEmail').value;
	let password = document.querySelector('#password').value;

	fetch('https://booking-system-kylecuizon73.herokuapp.com/api/users/login', {
		method : "POST",
		body : JSON.stringify({
					email : email,
					password : password
				}),
		headers : {
			'Content-Type' : 'application/json'
		}
	})
	.then( res => res.json())
	.then( data => {
		// console.log(data)
		if (data.accessToken){
		localStorage.setItem('token', `Bearer ${data.accessToken}`)


		fetch('https://booking-system-kylecuizon73.herokuapp.com/api/users/details', {
			headers : {
				Authorization : localStorage.getItem('token')
			}

		})

		.then( res => res.json ())
		.then (data => {
			console.log(data)

			localStorage.setItem("id", data._id)
			localStorage.setItem("isAdmin", data.isAdmin)
            window.location.replace("./courses.html")

		})

	} else {
		alert("Something went wrong.")
	}
	})
})