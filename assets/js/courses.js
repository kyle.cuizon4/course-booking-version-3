let adminUser = localStorage.getItem("isAdmin");
// fetch


if(adminUser === "true"){
fetch(url + '/api/admin')
.then( res => res.json())
.then(data => {
    // console.log(data) //array
  
    let buttonContainer = document.querySelector('#buttonContainer')
    let coursesContainer = document.querySelector('#coursesContainer');
    let profileContainer = document.querySelector('#profileContainer')
	let cardFooter = (course) => {
	    if( course.isActive == true ) {		
			return `
				
				<a href="./studentDetails.html?courseId=${course._id}" class="btn btn-warning text-white btn-block editButton" >Students List</a>
				<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton" >Edit</a>
				<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block dangerButton">Disable course</a>
			`
		} else {
			return `
			
			<a href="./studentDetails.html?courseId=${course._id}" class="btn btn-warning text-white btn-block editButton" >Students List</a>
			<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton" >Edit</a>
			<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block dangerButton">Activate Course</a>`
		}
	}
	let courseCard = (params) => (`
		<div class="col-md-4 my-3">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">${params.name}</h5>
					<p class="card-text text-left">
						${params.description}
					</p>
					<div class="card-text text-right">
						&#8369; ${params.price}
					</div>
				</div>
				<div class="card-footer">
					${cardFooter(params)}
				</div>
			</div>
		</div>
    `)
    
    let buttonCard = (`
            <h2 class="text-center">You are now logged in as Admin</h2>

           <h3 class="text-center"> You may now create, delete, edit and activate a course </h3>
        <main class="container">
        <div class="card d-flex justify-content-center mt-5">
            <div class="card-body">
                <div class="container my-5">	
                    <h2 class="text-center my-5">Create new course</h2>
                    <a href="./addCourse.html" class="btn btn-info text-white btn-block editButton" >Create a course</a>
                </div> 
            </div>
        </div> 
      
        </main>             
    `)

    
    let profileCont = (``)

	let courseData;
	if (data.length < 1){
		courseData = "No courses available";
	} else {
		courseData = data.map( course => {
			return courseCard(course);
		}).join("")
	}

    coursesContainer.innerHTML = courseData;
    buttonContainer.innerHTML = buttonCard
    profileContainer.innerHTML = profileCont
})

    

} else  {

    fetch(url + '/api/courses')
    .then( res => res.json())
    .then(data => {
        // console.log(data) //array
        let coursesContainer = document.querySelector('#coursesContainer');
        let cardFooter = (course) => {
                return `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Select Course</a>
                        <a href="./studentDetails.html?courseId=${course._id}" class="btn btn-warning text-white btn-block editButton" >Students List</a>
                `
        }

        let courseCard = (params) => (`
            <div class="col-md-4 my-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">${params.name}</h5>
                        <p class="card-text text-left">
                            ${params.description}
                        </p>
                        <div class="card-text text-right">
                            &#8369; ${params.price}
                        </div>
                    </div>
                    <div class="card-footer">
                        ${cardFooter(params)}
                    </div>
                </div>
            </div>
        `)
     

        let courseData;
        if (data.length < 1){
            courseData = "No courses available";
        } else {
            courseData = data.map( course => {
                return courseCard(course);
            }).join("")
        }
    
        coursesContainer.innerHTML = courseData;
       
        
    })
    
    
    
}

let profileName  = document.querySelector('#profileName')
let profileMobile = document.querySelector('#profileMobile')
let profileEmail = document.querySelector('#profileEmail')



fetch(`${url}/api/users/details/`, {


	headers : {
				Authorization : localStorage.getItem('token')
			}


}).then( response => response.json()).then(data => { 
	
	let enrollmentContainer = document.querySelector('#tryer')

	profileName.innerHTML = (data.firstName +"" +data.lastName)
	
	profileMobile.innerHTML = data.mobileNo
	profileEmail.innerHTML = data.email



	let enrollmentList;
	if (data.enrollments.length < 1 ){
		enrollmentList = "No courses."
	} else {

		enrollmentList = data.enrollments.forEach( course => {

			fetch(`${url}/api/courses/${course.courseId}`).then(response => response.json()).then(dataList => {
			
			
			// return profileEnrollments(course)
			enrollmentContainer.innerHTML += '<li>' + dataList.name, + '</li>';
				
			})

		}).join("")

	} 



})
