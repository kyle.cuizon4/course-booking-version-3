console.log(window.location.search)

let params = new URLSearchParams(window.location.search);

console.log(params.has('courseId'));

let courseId = params.get('courseId');

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")



fetch(url + `/api/courses/` +courseId)
.then( res => res.json())
.then( data => {
	console.log(data)
	name.value = data.name
	price.value = data.price
	description.value = data.description

	let editCourse = document.querySelector('#editCourse');
	editCourse.addEventListener("submit", e => {
		e.preventDefault();


		let nameVal = name.value;
		let priceVal = price.value;
		let descVal = description.value;

		fetch(`${url}/api/courses`, {
			method : 'PUT',
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : localStorage.getItem('token')
			},

			body: JSON.stringify({
				courseId,
				name : nameVal,
				description : descVal,
				price : priceVal
			})

		})
		.then(res => res.json ())
		.then( data => {
			if (data === true) {
				window.location.replace('./courses.html')
			} else {
				alert('Something went wrong.')
			}
		})
	})
})

