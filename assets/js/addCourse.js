let createCourse = document.querySelector('#createCourse')

createCourse.addEventListener('submit', e => {
	e.preventDefault();

	//get all field values

	let courseName  = document.querySelector('#courseName').value
	let coursePrice = document.querySelector('#coursePrice').value
	let courseDescription = document.querySelector('#courseDescription').value
	// send the data to the backend

	fetch(url + `/api/courses` , {
		method : 'POST',

		headers : {
			'Content-Type'  : 'application/json',
			'Authorization' : localStorage.getItem('token')
		},
		body : JSON.stringify({

			name : courseName,
			description : courseDescription,
			price : coursePrice
			
		})


	})
	.then(res => res.json())
	.then(data => {
		if (data === true){
			alert("created successfully")
			window.location.replace('./courses.html')
		} else {
			alert("something went wrong")
		}
	})
	// if the fetch result is true
	// redirect user to the course.html
	// else alert 
})